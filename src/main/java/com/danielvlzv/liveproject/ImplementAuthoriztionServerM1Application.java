package com.danielvlzv.liveproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImplementAuthoriztionServerM1Application {

	public static void main(String[] args) {
		SpringApplication.run(ImplementAuthoriztionServerM1Application.class, args);
	}

}
